from django.db import models

#TODO
#Project
#Task
#Comment

class Project(models.Model):

    project_name = models.CharField(max_length=100)
    project_description = models.CharField(max_lengh=500)

class Task(models.Model):

    status_choices = (
        'waiting', 
        'implementation', 
        'verifying',
        'releasing',
        )
    task_name = models.CharField(max_length=100)
    project = models.ForeignKey('Project', on_delete=models.CASCADE)
    task_description = models.TextField()
    status = models.CharField(max_length=20, choices=status_choices)

class Comment(models.Model):

    task = models.ForeignKey('Task', on_delete=models.CASCADE)
    comment = models.TextField()

